output "security_groups_ids" {
  description = "Ids of the created security_groups."
  value       = module.module_usage_howto.security_groups_ids
}

output "security_groups_arns" {
  description = "Arns of the created security_groups."
  value       = module.module_usage_howto.security_groups_arns
}

output "debug" {
  description = "For debug purpose."
  value       = module.module_usage_howto.debug
}
